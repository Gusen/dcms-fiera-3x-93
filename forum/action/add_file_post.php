<?php

$post = mysql_fetch_object(mysql_query('SELECT `id`, `id_theme` FROM `forum_posts` WHERE `id` = '.intval($_GET['post']).' AND `id_user` = '.$user['id']));
$forum = mysql_fetch_object(mysql_query('SELECT `id`, `access`, `name` FROM `forum` WHERE `id` = '.intval($_GET['forum'])));
$razdel = mysql_fetch_object(mysql_query('SELECT `id`, `id_forum`, `name` FROM `forum_razdels` WHERE `id_forum` = '.$forum->id.' AND `id` = '.intval($_GET['razdel'])));
$theme = mysql_fetch_object(mysql_query('SELECT `id`, `id_razdel`, `name` FROM `forum_themes` WHERE `id_razdel` = '.$razdel->id.' AND `id` = '.intval($_GET['theme'])));

if (!$theme || !$razdel || !$forum || !$post || ($forum->access == 1 && $user['group_access'] < 8) || ($forum->access == 2 && $user['group_access'] < 3)) {
    header('Location: '.FORUM);
    exit;
} else {
    $set['title'] = 'Добавление файлов';
    include_once '../sys/inc/thead.php';
    title().aut();

    if (isset($_POST['download'])) {
        $file = mysql_real_escape_string(stripcslashes($_FILES['file']['name']));
        $file = preg_replace('(\#|\?)', NULL, $file);
        $name = preg_replace('#\.[^\.]*$#', NULL, $file);
        $size = filesize($_FILES['file']['tmp_name']);
        $ras = strtolower(preg_replace('#^.*\.#', NULL, $file));
        if ($ras == 'php' || $ras == 'exe' || $ras == 'js' || $ras == 'html' || $ras == 'htaccess' || $ras == NULL) {
            ?>
            <div class = 'err'>Ошибка при выгрузке файла.</div>
            <?
        } else {
            $count_files = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_post_files` WHERE `id_post` = '.$post->id), 0);
            mysql_query('INSERT INTO `forum_post_files` SET `id_theme` = '.$theme->id.', `id_post` = '.$post->id.', `id_user` = '.$user['id'].', `name` = "'.$post->id.'_'.($count_files+1).'.'.$ras.'", `real_name` = "'.$file.'", `size` = '.$size);
            move_uploaded_file($_FILES["file"]["tmp_name"], 'files/'.$post->id.'_'.($count_files+1).'.'.$ras);
            $_SESSION['download'] = '<div class = "msg">Файл успешно прикреплён.</div>';
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/'.$post->id.'/add_file');
            exit;
        }
    } elseif (isset($_GET['del_file']) && (mysql_fetch_object(mysql_query('SELECT `id_user` FROM `forum_post_files` WHERE `id` = '.intval($_GET['del_file'])))->id_user == $user['id'] || user_access('forum_post_ed'))) {
        $_SESSION['download'] = '<div class = "msg">Файл успешно удалён.</div>';
        $file = mysql_fetch_object(mysql_query('SELECT `id`, `id_post`,  `name` FROM `forum_post_files` WHERE `id` = '.intval($_GET['del_file'])));
        unlink(H.FORUM.'/files/'.$file->name);
        mysql_query('DELETE FROM `forum_post_files` WHERE `id` = '.$file->id);
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/'.$post->id.'/add_file');
        exit;
    }
    if (isset($_SESSION['download'])) {
        echo $_SESSION['download'];
        unset($_SESSION['download']);
    }
    ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?= FORUM ?>'>Форум</a> / <a href = '<?= FORUM.'/'.$forum->id ?>/'><?= output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?= output_text($razdel->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?= output_text($theme->name, 1, 1, 0, 0, 0) ?></a>
    </div>
    <table class = 'post' cellspacing = '0' cellpadding = '0'>
        <?
        $files = mysql_query('SELECT * FROM `forum_post_files` WHERE `id_post` = '.$post->id);
        while ($p_file = mysql_fetch_object($files)) {
            $ras = strtolower(preg_replace('#^.*\.#', NULL, $p_file->name));
            if ($ras == 'jpg' || $ras == 'jpeg' || $ras == 'gif' || $ras == 'png' || $ras == 'bmp' || $ras == 'ico') {
                $icon = FORUM.'/files/'.$p_file->name;
            } elseif ($ras == '3gp' || $ras == 'mp4' || $ras == 'avi' || $ras == 'mpeg' || $ras == 'flv' || $ras == 'wmv' || $ras == 'mkv') {
                $icon = FORUM.'/icons/files/video.png';
            } elseif ($ras == 'docx' || $ras == 'doc' || $ras == 'docm' || $ras == 'dotx' || $ras == 'dot' || $ras == 'dotm') {
                $icon = FORUM.'/icons/files/word.png';
            } elseif ($ras == 'mp1' || $ras == 'mp2' || $ras == 'mp3' || $ras == 'wav' || $ras == 'aif' || $ras == 'ape' || $ras == 'flac' || $ras == 'ogg' || $ras == 'asf' || $ras == 'wma') {
                $icon = FORUM.'/icons/files/music.png';
            } elseif ($ras == 'zip' || $ras == 'rar' || $ras == 'tar' || $ras == '7-zip' || $ras == 'gzip' || $ras == 'jar' || $ras == 'jad' || $ras == 'war' || $ras == 'xar') {
                $icon = FORUM.'/icons/files/archive.png';
            } elseif ($ras == 'txt' || $ras == 'xml') {
                $icon = FORUM.'/icons/files/txt.png';
            } elseif ($ras == 'pdf') {
                $icon = FORUM.'/icons/files/pdf.png';
            } elseif ($ras == 'psd') {
                 $icon = FORUM.'/icons/files/psd.png';
            } else {
                $icon = FORUM.'/icons/files/file.png';
            }
            ?>
            <tr>
                <td class = 'p_t' style = 'width: 50px; border-right: none'>
                    <a href = '<?= $icon ?>'><img src = '<?= $icon ?>' alt = '' style = 'width: 50px; height: 50px' /></a>
                </td>
                <td class = 'p_t' style = 'border-left: none'>
                    <?= output_text($p_file->real_name) ?><br />
                    Размер файла: <?= size_file($p_file->size) ?><br />
                    [ <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/'.$post->id.'/del_file='.$p_file->id ?>'>Удалить файл</a> ]
                </td>
            </tr>
            <?
        }
        ?>
    </table>
    <form action = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/'.$post->id ?>/add_file' method = 'post' enctype = 'multipart/form-data' class="p_m">
        <input type = 'file' name = 'file'><br />
        <input type = 'submit' name = 'download' value = 'Добавить файл' />
    </form>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?= FORUM ?>'>Форум</a> / <a href = '<?= FORUM.'/'.$forum->id ?>/'><?= output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?= output_text($razdel->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?= output_text($theme->name, 1, 1, 0, 0, 0) ?></a>
    </div>
    <?
    include_once '../sys/inc/tfoot.php';
    exit;
}

?>